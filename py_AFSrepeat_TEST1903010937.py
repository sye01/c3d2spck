import numpy as np
import glob

afs_list = glob.glob(".\\14072017_999999m001_PAD_Dyn_Right_04*.afs")

text_header = "!  SIMPACK Array Function File\n"+"header.begin\n"+"  file.type    = 'afs'      ! SIMPACK File Type: Array Function Set\n"+"  file.version = 1.0        ! Release\n"+"header.end\n"+"\n"+"\n"+"arrfunc.begin\n"+"  type           = 'y(x)'               ! Input Function Type: Univariate Function y(x)\n"+"  name           = 'time'               ! Input Function Name\n"+"  eval.type      = 'linear'             ! Linear Interpolation\n"+"  x.unit         = 's'                  ! Unit x: time [s]\n"+"  y.unit         = 's'                  ! Unit y: time [s]\n"+"  data.begin\n"+"!   x                 y\n"
text_footer = "  data.end\n" + "arrfunc.end\n"

for afs_name in afs_list : 



	with open(afs_name,'r') as afs_file : 
		afs_truc = afs_file.readlines()

		vectRecto = np.empty((0,1))
		vectVerso = np.empty((0,1))

		for afs_line in afs_truc :
			try:
				elem = float(afs_line.split()[1])
				vectRecto = np.vstack((vectRecto,elem))
				vectVerso = np.vstack((elem,vectVerso))
			except:
				pass

		truc = np.vstack((vectRecto,vectVerso))
		truc = np.vstack((truc,truc,truc,truc))
		vect = np.vstack((truc,truc,truc,truc))
		time = np.arange(vect.shape[0])*0.005

		f = open(afs_name[:-4]+"_repeat.afs","w")
		f.write(text_header)
		for j in range(0,vect.shape[0]):
			f.write("    {0:.10e}".format(float(time[j])))
			f.write("  {0:.10e}\n".format(float(vect[j])))
		f.write(text_footer)
		f.close()
