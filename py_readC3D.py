import numpy as np
import btk


class C3D2TMP :
    
    def __init__(self, C3D_nameList, MRK_nameList) :
        
        #INIT
        self.threshold_GRFz = 10 #N
        self.C3D_nameList = C3D_nameList
        self.MRK_nameList = MRK_nameList
        self.content_per_file = self.get_content()
    
        self.write_csv_files_positions()
        self.write_csv_files_velocities()
        
    
    
    
    def get_content(self) :
        content_per_file = {}
        for C3D_name in self.C3D_nameList :
    
            # READ C3D FILES
            reader=btk.btkAcquisitionFileReader()
            reader.SetFilename(C3D_name + ".c3d")
            reader.Update()
            content_per_file[C3D_name] = reader.GetOutput()
            
        return content_per_file
    




    
    def get_GRFz(self, acq) :
        
        #INIT                                
        GRF_values = []
        GRF_embedded_list = acq.GetAnalog("Fz1").GetValues()
          
        for sub_list in GRF_embedded_list :
            for val in sub_list :
                GRF_values.append(-val)
                
        return GRF_values
    
    
    
    

    def get_index_contact(self, acq):
        
        #INIT
        GRF_values = self.get_GRFz(acq)                
        threshold_GRFz = self.threshold_GRFz
        nb_values_below_threshold = 0        
        nb_values_above_threshold = 0   
        first_index = 0

        for index, value in enumerate(GRF_values) :
            if(value > threshold_GRFz) :
                if (nb_values_below_threshold == 1) :
                    first_index =  index - nb_values_below_threshold - 1
                nb_values_below_threshold += 1

                
            if (first_index != 0):                                             #to start after the mesurement of the initial time
                if (value < threshold_GRFz) :                                      
                    if (nb_values_above_threshold == 5):                       #to be sure that the next consecutive 5 values respond to noise
                        last_index = index - nb_values_above_threshold   
                    nb_values_above_threshold += 1
        
        return first_index, last_index

    



        
    def write_csv_files_positions(self) :
        
        for C3D_name in self.content_per_file :
            
            acq = self.content_per_file[C3D_name]
            first_index, last_index = self.get_index_contact(acq)
                        
            Mcsv = np.empty((acq.GetPointFrameNumber(),0))
            
            for MRK_name in self.MRK_nameList: 
        
                # READ MRK POINTS
                MRK = acq.GetPoint(MRK_name).GetValues()
                Mcsv = np.hstack((Mcsv,MRK))
              
            Mcsv = Mcsv[first_index:last_index,:]
            np.savetxt("CSV/{}_TMP.csv".format(C3D_name),Mcsv,delimiter=",")




    def write_csv_files_velocities(self) :
        
        for C3D_name in self.content_per_file :
            
            acq = self.content_per_file[C3D_name]
            period = 1.0 / acq.GetAnalogFrequency()
            
            first_index, last_index = self.get_index_contact(acq)
            Mcsv = np.empty((acq.GetPointFrameNumber(),0))
            
            for MRK_name in self.MRK_nameList: 
        
                # READ MRK POINTS
                MRK = acq.GetPoint(MRK_name).GetValues()
                MRK_vel = np.append(np.zeros((1,3)), np.diff(MRK, axis = 0)/period, axis = 0 )
                Mcsv = np.hstack((Mcsv,MRK_vel))           

            Mcsv = Mcsv[first_index:last_index,:]
            np.savetxt("CSV/{}_TMP_VEL.csv".format(C3D_name), Mcsv, delimiter=",")
                
