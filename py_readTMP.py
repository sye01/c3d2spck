import numpy as np
import csv

def TMP2SVs(C3D_nameList,MRK_nameList):
    file_header = open("MBS_macro_v.header.spck","r")
    file_footer = open("MBS_macro_v.footer.spck","r") 
    text_header = file_header.read() 
    text_footer = file_footer.read() 
    i = 0

    for C3D_name in C3D_nameList:
        with open(C3D_name + "_TMP.csv") as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            initrow = next(readCSV)

        f = open(C3D_name + "_Rizzoli0.spck","w")
        f.write(text_header)
        for MRK_name in MRK_nameList:
            # RIZZOLI'S MARKERS
            f.write("subvar.str (                        $_Rizzoli_"+MRK_name+"_x              ) = '")
            f.write("  {0:.10e}".format(float(initrow[i])))
            f.write("'                     ! Definition\n\n")
            i = i+1
            f.write("subvar.str (                        $_Rizzoli_"+MRK_name+"_y              ) = '")
            f.write("  {0:.10e}".format(float(initrow[i])))
            f.write("'                     ! Definition\n\n")
            i = i+1
            f.write("subvar.str (                        $_Rizzoli_"+MRK_name+"_z              ) = '")
            f.write("  {0:.10e}".format(float(initrow[i])))
            f.write("'                     ! Definition\n\n")
            i = i+1
        f.write(text_footer)
        f.close()



def TMP2AFS(C3D_nameList,MRK_nameList): 

    text_header = "!  SIMPACK Array Function File\n"+"header.begin\n"+"  file.type    = 'afs'      ! SIMPACK File Type: Array Function Set\n"+"  file.version = 1.0        ! Release\n"+"header.end\n"+"\n"+"\n"+"arrfunc.begin\n"+"  type           = 'y(x)'               ! Input Function Type: Univariate Function y(x)\n"+"  name           = 'time'               ! Input Function Name\n"+"  eval.type      = 'linear'             ! Linear Interpolation\n"+"  x.unit         = 's'                  ! Unit x: time [s]\n"+"  y.unit         = 's'                  ! Unit y: time [s]\n"+"  data.begin\n"+"!   x                 y\n"
    text_footer = "  data.end\n" + "arrfunc.end\n"

    for C3D_name in C3D_nameList:

        # READ TMP FILES
        M = np.empty((0,3*len(MRK_nameList)))

        with open("CSV/{}_TMP.csv".format(C3D_name)) as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            for row in readCSV:
                M = np.vstack((M,row))

        t = np.arange(M.shape[0])*0.005

        i = 0

        for MRK_name in MRK_nameList:

            # EXTRACTING X FUNTION    
            f = open("AFS/{}{}_x.afs".format(C3D_name, MRK_name),"w")
            f.write(text_header)
            for j in range(0,M.shape[0]):
                f.write("    {0:.10e}".format(t[j]))
                f.write("  {0:.10e}\n".format(float(M[j,i])))
            f.write(text_footer)
            f.close()
            i = i+1

            # EXTRACTING Y FUNTION
            f = open("AFS/{}{}_y.afs".format(C3D_name, MRK_name),"w")
            f.write(text_header)
            for j in range(0,M.shape[0]):
                f.write("    {0:.10e}".format(t[j]))
                f.write("  {0:.10e}\n".format(float(M[j,i])))
            f.write(text_footer)
            f.close()
            i = i+1

            # EXTRACTING Z FUNTION
            f = open("AFS/{}{}_z.afs".format(C3D_name, MRK_name),"w")
            f.write(text_header)
            for j in range(0,M.shape[0]):
                f.write("    {0:.10e}".format(t[j]))
                f.write("  {0:.10e}\n".format(float(M[j,i])))
            f.write(text_footer)
            f.close()
            i = i+1
