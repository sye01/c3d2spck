from py_readC3D import C3D2TMP
from py_readTMP import TMP2IF2

C3D_nameList = ["14072017_999999m001_PAD_Dyn_Right_04","14072017_999999m001_PAD_Dyn_Right_05","14072017_999999m001_PAD_Dyn_Right_06","14072017_999999m001_PAD_Dyn_Right_07","14072017_999999m001_PAD_Dyn_Right_08"]
Macro_List = ["RIPS","RIAS","RFTC","RFLE","RFME","RFAX","RTTC","RFAL","RTAM","LIPS","LIAS","LFTC","LFLE","LFME","LFAX","LTTC","LFAL","LTAM"] #18
RFoot_list = ["RFCP","RFCD","RFMT","RFST","RFNT","RFM1","RFM2","RFM5","RPD6","RFMB","RSMB","RFPT"] #12
LFoot_list = ["LFCP","LFCD","LFMT","LFST","LFNT","LFM1","LFM2","LFM5","LPD6","LFMB","LSMB","LFPT"] #12
MRK_nameList = Macro_List+RFoot_list+LFoot_list

C3D2TMP(C3D_nameList,MRK_nameList)
TMP2IF2(C3D_nameList,MRK_nameList)
