from py_readC3D import C3D2TMP
from py_readTMP import TMP2AFS
from py_readTMP import TMP2SVs


#id_file = "14072017_999999m001_PAD_Dyn_Right_0"                                #PA
id_file = "20012017_999999m005_PAD_Dyn_Right_0"                                 #ANDREA
index = range(3,9)
C3D_nameList = []
for i in index :
    C3D_nameList.append("{}{}".format(id_file,str(i)))

print C3D_nameList


##OLD NAMES
#Macro_List = ["RIPS","RIAS","RFTC","RFLE","RFME","RFAX","RTTC","RFAL","RTAM","LIPS","LIAS","LFTC","LFLE","LFME","LFAX","LTTC","LFAL","LTAM"] #18
#RFoot_list = ["RFCP","RFCD","RFMT","RFST","RFNT","RFM1","RFM2","RFM5","RPD6","RFMB","RSMB","RFPT"] #12
#LFoot_list = ["LFCP","LFCD","LFMT","LFST","LFNT","LFM1","LFM2","LFM5","LPD6","LFMB","LSMB","LFPT"] #12

#NEW NAMES
Macro_List = ["FM_RIPS","FM_RIAS","FM_RFTC","FM_RFLE","FM_RFME","FM_RFAX","FM_RTTC","FM_RFAL","FM_RTAM","FM_LIPS","FM_LIAS","FM_LFTC","FM_LFLE","FM_LFME","FM_LFAX","FM_LTTC","FM_LFAL","FM_LTAM"] #18
RFoot_list = ["FM_RFCP","FM_RFCD","FM_RFMT","FM_RFST","FM_RFNT","FM_RFM1","FM_RFM2","FM_RFM5","FM_RPD6","FM_RFMB","FM_RSMB","FM_RFPT"] #12
LFoot_list = ["FM_LFCP","FM_LFCD","FM_LFMT","FM_LFST","FM_LFNT","FM_LFM1","FM_LFM2","FM_LFM5","FM_LPD6","FM_LFMB","FM_LSMB","FM_LFPT"] #12



MRK_nameList = Macro_List + RFoot_list + LFoot_list

C3D2TMP(C3D_nameList,MRK_nameList)
TMP2AFS(C3D_nameList,MRK_nameList)
##TMP2SVs(C3D_nameList,MRK_nameList)
