# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 11:24:11 2019

@author: Rosine Desmet
"""

    def write_csv_files(self) :
        
        for C3D_name in self.content_per_file :
            
            acq = self.content_per_file[C3D_name]
            first_index, last_index = self.get_index_contact(acq)
                                    
            for MRK_name in self.MRK_nameList: 
        
                # READ MRK POINTS
                MRK = np.array(acq.GetPoint(MRK_name).GetValues())
                print np.array(MRK)
                print np.array(MRK).shape
#                
                MRK = MRK[first_index:last_index,:]
                                
#                print Mcsv.shape
                np.savetxt("CSV/{}_TMP.csv".format(C3D_name),MRK,delimiter=",")